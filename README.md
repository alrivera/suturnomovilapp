# README #

Migración aplicación suTurnoMovil

# Instalación #

# Pasos de la instalación: #

## Clonando la app del repositorio ##
	- mkdir turnoMovil
	- cd turnoMovil
	- git clone https://usuario@bitbucket.org/desarrollog3/turnomovil.git

## Configurando dependencias ##
	- cordova plugin add cordova-plugin-compat
	- cordova plugin add cordova-plugin-geolocation
	- cordova plugin add cordova-plugin-network-information
	- cordova plugin add cordova-plugin-splashscreen
	- cordova plugin add cordova-plugin-statusbar
	- cordova plugin add cordova-plugin-whitelist

# Estado de la versión: #

# Global: # 

	- Recuperar contraseña cuando login incorrecto
	- No marca pestaña oficina al comienzo
	- en pedir numero las letras estan muy grandes
	- los tiempos en lista sucursales dice: 0 min debe decir <5 min
	- boton regrese muy chico

	- Ms grande la tipografia de los titulos, mas aire (ver servicios) (ok)
	- sacar negrita encebezado (ok)
	- column distancia mismo tamaño titulos (ok)
		cambiar espera por kilometraje (ok)
		queda el circulo corrido (ok)
	- volver en encabezado mas grande la flecha (ok)
	- Seleccione Sucursal (mayus) (ok)
	- pedir numero, tamaño Horarios atencion y logo mas grande	(ok)
	- linea tabla blanco y encabezado centrado (ok)
	- pestañas cambiar atencion x su numero (ok)
	- en mostrar ticket mas abajo el reloj y atendiendo (ok)
	- boton cancelar me atendieron (ok)
	- si tengo numero no puedo volver al inicio (ok)
	- cambiar error al hacer login (ok)
	- menos de 5 minutos o 5 personas, envia notificacion.
	- si se pasa el numero envia msg y bota al inicio

	- Encabezado: En blanco y no negritas (ok)
	- fondo azul con diagonal se sale (ok)
	- poner icono transparente a izquierda en primera pagina (ok)
	- Achicar tipografia (ok)
	- distancia espacio km y guión parada espera (ok)
	- alto logos en 32 (ok)
	- azules todos iguales (ok)
	- márgenes dentro de mapa
	- alert de no es posible tomar cambiar alert por msg correcto (ok)
	- Validaciones y mensajes al ingresar pin
	- Validar fuera de horario
	- Al pedir ticket, columna tiempo alineada a la derecha (ok)
	- botones nabber cambiar left a flecha limpia, y botón sin bordes (ok)
	- Validación pasa sin rut (no validó)

	- Conectar Login por Google y Facebook --
	- Conectar Registro y Login --
	- boton logout (ok)
	- Splash screen copiado del original (ok)
	- revisar tipografia usada anteriormente (ok)
	- quitarle margenes a la hoja (ok)
	- fondo blanco a las hojas (ok)
	- Activar control de desconexion de la red (ok)
	- Mostrar mensaje cuando no hay conexión a internet (ok)

# Lista empresas
	- div color fondo blanco (ok)
	- Falta "Mi Último Número" (ok)
	- Falta reloj
	- Incorpara mi ultimo numero al menu

# Lista servicios de la empresa
	- header 
		"opciones de barra top a la derecha"
	- Pie de pagina
		"Seleccione un Servicio" (ok)

# Oficinas
	- cambiar "sucursales" x nombre de servicio seleccionado (ok)
	- Titulo negrita (ok)
	- direccion sin negrita (ok)
	- Pie de pagina (ok)
		"Seleccione Sucursal" (ok)
		"* Tiempo estimado de espera" (ok)
	- Esconder distancia cuando es corta


# Obtiene Ticket
	- Titulo fondo azul con logo (ok)
	- Falta direccion oficina (ok)
	 	- Tiempo estimado de espera derecha (ok)
	 	- direccion abajo (ok)
	- Mensaje 1 y 2 en fondo azul (ok)
	- Mapa esconder filtros y encabezado y mostrar ruta (ok)
	- pestañas: Ubicación, Galería, Oficina (ok)
	- No se ve reloj ni atendiendo (ok)
	- NUmero obtenido a la derecha del logo empresa (ok)
	- recordar pin 
	- Colores de los botones cancelar y me atendieron en azul
	- botón me atendieron: 
		"¿Confirma que ya fue atendido?" (ok)
		"¿Está seguro que desea cancelar su turno?" (ok)
		"¡Pronto será tu turno, acércate al punto de atención!" (probarlo)
	- al cancelar o me atendieron boton volver debe aparecer (ok)