
var mouseIsDown = false;

canvas.onmousedown = function(e){
    dragOffset.x = e.x - mainLayer.trans.x;
    dragOffset.y = e.y - mainLayer.trans.y;

    mouseIsDown = true;

    // $('.pulltorefresh').show();
    console.log("debo actualizar");
}
canvas.onmouseup = function(e){
    if(mouseIsDown) mouseClick(e);

    mouseIsDown = false;
}

canvas.onmousemove = function(e){
    if(!mouseIsDown) return;

    mainLayer.trans.x = e.x - dragOffset.x;
    mainLayer.trans.y = e.y - dragOffset.y;
    return false;
}

function mouseClick(e){
    // click action
}