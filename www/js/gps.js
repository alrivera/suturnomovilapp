// var persona = $('[ng-controller="mainController"]').scope().persona;

function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;

    var centerLatitude = parseFloat(localStorage.getItem("centerLatitude")) || 33;
    var centerLongitude = parseFloat(localStorage.getItem("centerLongitude")) || -44;

    var centro = {lat: centerLatitude, lng: centerLongitude};

    var map = new google.maps.Map(document.getElementById('divMap2'), {
        zoom: 16,
        center: centro
    });
    directionsDisplay.setMap(map);

    calculateAndDisplayRoute(directionsService, directionsDisplay);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {

    if (navigator.geolocation) {
        navigator.geolocation.watchPosition (
            function(position) {
                var origen = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                console.log(origen);
                localStorage.setItem("gps_coords", JSON.stringify(origen));

                var lat_dest = parseFloat(localStorage.getItem("centerLatitude"));
                var lng_dest = parseFloat(localStorage.getItem("centerLongitude"));
                directionsService.route({
                    origin: origen,
                    destination: {lat: lat_dest, lng: lng_dest},
                    travelMode: google.maps.TravelMode.TRANSIT,
                    optimizeWaypoints: true
                }, function(response, status) {
                    if (status === google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                    } else {
                        console.log('Error al calcular la ruta: ' + status);
                    }
                });
            }, function(error) {
                console.log("no puedo obtener posicion actual del gps");
            }
      );
    };
}
