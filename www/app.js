var application = angular.module('turnoMovil', []);

application.config(['$httpProvider', function($httpProvider) {
	// $httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

application.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                console.log('imagen cargada');
            });
            element.bind('error', function(){
                console.log('imagen no cargada');
            });
        }
    };
});

application.controller('mainCtrl', ['$scope', '$http', '$q', '$cacheFactory', function ($scope, $http, $q, $cacheFactory) {

	console.log("main Controller cargado");

	$scope.config = {
    	titulo: "titulo",
    	// apiurl: "http://localhost:82",
    	// apiurl: "http://gear3:82",
    	// apiurl: "http://186.103.164.3:5902", //Producción TTP
    	apiurl: "http://186.103.164.3:5903", //Calidad / DesaTTP
    	tiempo_actualizacion: 30000,
    	tiempo_menos_de: 5,
    	cierra_atencion_hrs: 8
    };

    //localStorage.setItem("logeado", "false");
    $scope.logeado = localStorage.getItem("logeado");
	
    if($scope.logeado == "false") {
    	setTimeout(function() {
    		$.mobile.pageContainer.pagecontainer("change", "#servicios", {
		        transition: "slidefade",
		        reverse: false
		    });	
    	}, 100);
    }

	// $('body').addClass('ui-loading');
    // $.mobile.showPageLoadingMsg('a', 'Un momento...', false);

	$scope.showBotVolver = false;

	$scope.cron = 0;

	$scope.servicios = [];
	$scope.serviciosLocal = [];
	$scope.localCache = [];

	$scope.showMsg = false;
	$scope.session_terminada = false;
	$scope.ultimaAtencion = null;
	$scope.atencion_terminada = false;
	$scope.bloqueaAlerta = false;
	
	$scope.tee = localStorage.getItem("tee"); 
	if(!$scope.tee) $scope.tee = "N/D";
    $scope.tee2 = localStorage.getItem("tee2");
    if(!$scope.tee2) $scope.tee2 = "N/D";

    $scope.pestanaActual = "oficina2";
    $scope.mapaCargado = false;
    $scope.requicitos = "";
    $scope.bloqueaTomaNumero = false;

    $scope.formu = {};
	$scope.formu.userRut = "";
    $scope.formu.numCelular = "";
    $scope.formu.numFijo = "";
    $scope.formu.numOrden = "";

    $scope.lat = localStorage.getItem('gps_latitud');
    $scope.lon = localStorage.getItem('gps_longitud');

    $scope.showUltimaAtencion = false;
    getUltimaAtencion();
	
	$scope.form = {};
	$scope.form.email = localStorage.getItem("email");
	// $scope.form.password = "";
	$scope.form.push_id = "null";
	$scope.nNumeroActual = "";

	$scope.area_ant = "";
	$scope.miNumero = "---";
	$scope.tea = "--";
	$scope.showTicket = false;
	$scope.showPideTicket = true;
	$scope.showBotVolver = true;
	$scope.atencionCancelada = false;

	$scope.letraServicioActualTurnoActual = "N/D";
	$scope.tee = "N/D";
	$scope.tee2 = "N/D";

	var reloj;

	// actualiza el cache y la lista si se modifica en el servicio
	// $scope.servicios = JSON.parse(localStorage.getItem('servicios'));
	$scope.serviciosUpdated = false;
	$scope.$watch('serviciosUpdated', function() {
        var serviciosNew = JSON.parse(localStorage.getItem('servicios'));
        if(serviciosNew != $scope.servicios) {
        	$scope.servicios = serviciosNew;
        }
    });

	// actualiza cada n segundos, si aplica
    $scope.actualiza = function() {

    	$scope.muestraTee = localStorage.getItem('muestraTee');

    	// solo actualiza si tiene numero
    	if($scope.miNumero != "---") {

    		console.log("actualizo con mumero");
    		$scope.showBotVolver = false;

			reloj = setTimeout(function() {
				var ticket_actual = localStorage.getItem('ticket_actual');
				if(ticket_actual != "---") {
					$scope.meAtendieron();
				}
		    }, $scope.config.cierra_atencion_hrs * 60 * 60 * 1000); 

    		// lee datos de ws status
    		var formData = {
    			/*Se llama directo al local storage para obtener la empresa y el servicio*/
		    	empresa_id: localStorage.getItem('empresa_id_actual'),//$scope.glob_empresa_id,
				sucursal_id: $scope.sucursal_actual,
				servicio_id: localStorage.getItem('servicio_id_actual'),//$scope.glob_servicio_id,
				numero: $scope.nNumeroActual
		    };
		    if(angular.element('#conectado').val() != "desconectado") {
		    	// ws para status
			    $http({
				    method: 'POST',
				    url: $scope.config.apiurl + "/status",
				    data: formData,
				    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				    transformRequest: function(obj) {
						var str = [];
						for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
				    }
				})
				.then(
					function(resp) {
						var r = resp.data;
						// $scope.tee = r.tee + "m";

						$scope.menos_de = "";

						$scope.turnoActual = r.numero_actual;
						$scope.letraServicioActual = r.letra_servicio;
						$scope.letraServicioActualTurnoActual = r.letra_servicio + "" + r.numero_actual;
						$scope.showBotVolver = false;

						if(r.tee == "-1") {
				        	$scope.tee = "F/H";
				        	$scope.tee2 = "F/H";
				        	$scope.letraServicioActualTurnoActual = "N/D";
				        } else {
				        	if(r.tee < $scope.config.tiempo_menos_de) {
					        	$scope.tee = "<"+$scope.config.tiempo_menos_de+"m";
					        	$scope.tee2 = $scope.config.tiempo_menos_de + " min";
					        	$scope.menos_de = "Menos de ";
					        } else {
					        	if($scope.tee === null || $scope.tee === "") {
					        		swal({
						                title: "Su Turno Móvil!",
						                text: "Problemas al obtener el tiempo de espera",
						                type: "error",
						                //html: true
						            });
					        		$scope.tee = "N/D";
					        		$scope.tee2 = "N/D";
					        		$scope.letraServicioActualTurnoActual = "N/D";
					        	} else {
					        		$scope.tee = r.tee + "m";
									$scope.menos_de = "";
									$scope.tee2 = r.tee + " min";	
					        	}
					    	}
					    	$scope.teeMin = r.tee;
				        } 
				        localStorage.setItem("tee", $scope.tee);
				        localStorage.setItem("tee2", $scope.tee2);

				        if($scope.bloqueaAlerta === false && $scope.atencion_terminada === false && $scope.atencionCancelada === false) {
				        	
				        	console.log("nNumeroActual");
					        console.log($scope.nNumeroActual);
					        console.log("turnoActual");
					        console.log($scope.turnoActual);
					        console.log("tee");
					        console.log(r.tee);

					        console.log($scope.turnoActual +">"+ $scope.nNumeroActual + "(" +r.tee);

					        if($scope.nNumeroActual == $scope.turnoActual && r.tee >= 0) { // && r.tee >= 0
					        	$scope.menos_de = "";
					        	$scope.tee = 0;
					        	$scope.tee2 = '0 min';
					        	swal({
					                title: "Su Turno Móvil!",
					                text: "Este número está siendo llamado",
					                type: "success",
					                //html: true
					            });
						    } else if($scope.nNumeroActual == $scope.turnoActual && r.tee == -1) { // && r.tee >= 0
						    	swal({
					                title: "Su Turno Móvil!",
					                text: "Este número ya ha sido atendido",
					                type: "success",
					                //html: true
					            });
					            $scope.meAtendieron();
						    } else if($scope.turnoActual > $scope.nNumeroActual) { // && r.tee == "-1" // falta atendiendo
					        	swal({
					                title: "Su Turno Móvil!",
					                text: "Este número ya ha sido atendido",
					                type: "success",
					                //html: true
					            });
					            $scope.meAtendieron();
					        } else {
					        	
						        if(r.tee < $scope.config.tiempo_menos_de || ($scope.nNumeroActual - $scope.turnoActual) < 5 ) { // falta atendiendo
						        	swal({
						                title: "Su Turno Móvil!",
						                text: "Pronto será tu turno, acércate al punto de atención",
						                type: "success",
						                //html: true
						            });
						        }
						    }	
					        
				        }
					},
					function(error) {
						swal({
				                title: "Su Turno Móvil!",
				                text: "Problemas al obtener Status, ¿Falta de parámetros?",
				                type: "error",
				                //html: true
				            });
						$scope.menos_de = "";
						$scope.tee = "N/D";
		        		$scope.tee2 = "N/D";
		        		$scope.letraServicioActualTurnoActual = "N/D";
					}
				);
			} else {
				// sin conexion
				swal({
		                title: "Su Turno Móvil!",
		                text: "Sin conexion a Internet",
		                type: "warning",
		                //html: true
		            });
				$scope.tee = "N/D";
	        	$scope.tee2 = "N/D";
	        	$scope.letraServicioActualTurnoActual = "N/D";
	        	$scope.menos_de = "";
			}; // if sin conexion
    	} else {

    		// lee datos de la sucursal
    		console.log("actualizo sin mumero");
    		if($scope.glob_empresa_id && $scope.glob_servicio_id) { // && $scope.atencion_terminada === false
    			
    			var lat = localStorage.getItem("gps_latitud");
    			var lon = localStorage.getItem("gps_longitud");
    			//Obtengo la información del local Storage
    			var empresaId = localStorage.getItem('empresa_id_actual');
				var servicioId = localStorage.getItem('servicio_id_actual');

    			if(lat != null && lat != "null" && lat != "undefined") { // tengo todos los parametros
		    		$http.get($scope.config.apiurl + "/sucursales/servicio/" + empresaId + "/" + servicioId + "/"+lat+"/"+lon)
				    .then(function(response) {
				        var r = response.data;
				        

				        if(r && r != null && r != "undefined") {
				        	
				        	var tee = r[$scope.direccionKey];

				        	if(tee != null && tee != "undefined") {
				        		$scope.teeMin = tee;
				        		if(r[$scope.direccionKey].tee == "-1") {
						        	$scope.tee = "F/H";
						        	$scope.tee2 = "F/H";
						        	$scope.letraServicioActualTurnoActual = "N/D";
						        } 
						        if(r[$scope.direccionKey].tee <= 5 && r[$scope.direccionKey].tee >= 0) {
							        	$scope.tee = "<5m";
							    } 
							    if(r[$scope.direccionKey].tee >= 5) {
							        	$scope.tee = r[$scope.direccionKey].tee + "m";
						        }
						        if(r[$scope.direccionKey].tee === null || r[$scope.direccionKey].tee === "" || r[$scope.direccionKey].tee == "undefined") {
						        	if (!swal.isVisible()) {
							        	swal({
							                title: "Su Turno Móvil!",
							                text: "Problemas al obtener el tiempo de espera [2]",
							                type: "error",
							                //html: true
							            });
							        }
						        	$scope.tee = "N/D";
						        	$scope.tee2 = "N/D";
						        	$scope.letraServicioActualTurnoActual = "N/D";
						        }	
				        	}
				        	localStorage.setItem("tee", $scope.tee);
				        	localStorage.setItem("tee2", $scope.tee2);
				    	}else
				    	{
				    		if (!swal.isVisible()) {
					    		swal({
					                title: "Su Turno Móvil!",
					                text: "Sin respuesta del servidor",
					                type: "error",
					                //html: true
					            });
					    	}
				    	}
				    });
				} else {
					// no tengo todos los paramentros, no puedo estimar tee
					if (!swal.isVisible) {
						swal({
				                title: "Su Turno Móvil!",
				                text: "No se puede estimar [tee] faltan parámetros",
				                type: "error",
				                //html: true
				            });
					}
					$scope.tee = "N/D";
		        	$scope.tee2 = "N/D";
		        	$scope.letraServicioActualTurnoActual = "N/D";
		        	$scope.teeMin = -1;
				}
			}
    	}
    }

    // revisa si existe atencion vigente
	var ticket_actual = localStorage.getItem('ticket_actual');
	if(ticket_actual !== "---" && ticket_actual !== null) {

		$scope.miNumero = ticket_actual;
		$scope.letra = localStorage.getItem('letra');
		$scope.numero_actual = localStorage.getItem('numero_actual');

		$scope.servicio_id_actual = localStorage.getItem('servicio_id_actual');
		$scope.empresa_id_actual = localStorage.getItem('empresa_id_actual');
		$scope.nNumeroActual = localStorage.getItem('nNumeroActual');
		// libera navegacion antior si existe
		if(localStorage.getItem('direccion_actual')) {
			var direccion = JSON.parse(localStorage.getItem('direccion_actual'));	
		
			$scope.direccion_actual = direccion;
			$scope.horario = direccion.horario;
	    	$scope.sucursal_actual = direccion.id;
	    	$scope.mensaje1 = direccion.mensaje1;
	    	$scope.mensaje2 = direccion.mensaje2;
	    	$scope.galeria = direccion.galeria;
	    	$scope.logo = localStorage.getItem("logo");
	    	$scope.nNumeroActual = localStorage.getItem("nNumeroActual");
	    	$scope.distanciaActual = localStorage.getItem('distanciaActual');
	    }

    	$scope.showTicket = true;
    	$scope.showPideTicket = false;
    	$scope.showBotVolver = false;

    	// console.log("cargo mapa y calculo la ruta");
    	var timestamp = new Date().getTime();
    	$scope.programa_mapa_ticket = "mapa.html?" + timestamp;

    	$scope.actualiza();

    	setTimeout(function() {
			$('#ticket').page();
			$("#t1").removeClass("ui-btn-active");
			$("#t2").removeClass("ui-btn-active");
			$("#t3").removeClass("ui-btn-active");
			$("#t4").removeClass("ui-btn-active");
			$("#t2").addClass("ui-btn-active");
			$('#ticket').page();
		    $("#tabs").tabs( "option", "active", 1 );

			$.mobile.pageContainer.pagecontainer("change", "#ticket", {
		        transition: "slidefade",
		        reverse: false
		    });
		
			$scope.$apply();
		}, 1);
	}

	$scope.goTicket = function() {
		$.mobile.pageContainer.pagecontainer("change", "#ticket", {
	        transition: "slidefade",
	        reverse: false
	    });
		$('#ticket').page();
		$("#t1").removeClass("ui-btn-active");
		$("#t2").removeClass("ui-btn-active");
		$("#t3").removeClass("ui-btn-active");
		$("#t4").removeClass("ui-btn-active");
		$("#t2").addClass("ui-btn-active");
		$('#ticket').page();
	    $("#tabs").tabs( "option", "active", 1 );
	}

	function CheckScopeBeforeApply() {
	    if(!$scope.$$phase) {
	         $scope.$apply();
	    }
	};

	// carga la lista de servicios durante el inicio de la app
	$scope.serviciosRefresh = function() {

		console.log("cargando servicios (1)");

		if(!ticket_actual || ticket_actual == null || ticket_actual == "undefined") {
			console.log("cargo servicios...");
			// $('body').addClass('ui-loading');

			var localCache = [];

			var serviciosCache = JSON.parse(localStorage.getItem('servicios'));

			if(serviciosCache) {
				console.log("cargo desde cache");
				$scope.servicios = JSON.parse(localStorage.getItem('servicios'));
			}

			$scope.isOnline = true;
			$http({
				method: 'GET',
				url: $scope.config.apiurl + "/servicios",
				cache: true,
				timeout: 15000
			})
			.then(
				function(response) { // success
		        
		        	$scope.isOnline = true;
					var localCacheRemoto = [];
				
					angular.forEach(response.data, function(valor, key) {
						var row = JSON.parse(valor);
						localCacheRemoto.push({rubro_id: row.rubro.id, rubro: row.rubro.nombre});
						
						angular.forEach(row.empresas, function(empresa, key2) {
							localCacheRemoto.push({rubro_id: row.rubro.id, rubro: '', empresa_id: empresa.id, id: empresa.id, nombre: empresa.nombre, logo: empresa.logo_data, logo_data: empresa.logo_data});	
						});
						localStorage.setItem('servicios', JSON.stringify(localCacheRemoto));
						$scope.serviciosUpdated = true;
					   	$scope.servicios = JSON.parse(localStorage.getItem('servicios'));
					   	setTimeout(function() {
					   		$scope.$apply();	
					   	},1);
					});

					// setTimeout(function() {
					// 	// actualiza contenido si cache es distinto que los datos del ws
				 //        if(localCacheRemoto && JSON.stringify(serviciosCache) != JSON.stringify(localCacheRemoto)) {
				 //        	console.log("actualizo el cache y lista, por ser distinto a la data del ws");
				        	
				 //        	// setTimeout(function() {
					// 	    	$scope.$apply(function() {
					// 			  	$scope.serviciosUpdated = true;
					// 	    		$scope.servicios = JSON.parse(localStorage.getItem('servicios'));
					// 	    		// console.log("debo quitar el mensaje actualizando");
					// 	    		// $('.pulltorefresh').hide();
					// 			});
					//     	// }, 1);
					//     }
					// }, 8000);
					// $('body').removeClass('ui-loading');

					setTimeout(function() {
						mRefresh.resolve();
					}, 2000);
					
				    swal.close();
					
			    }, function(data) { //error
			    	
			    	console.log("timeout servicios");
			    	if($scope.logeado) {
						$scope.isOnline = false;
						$('body').removeClass('ui-loading');
						$scope.servicios = JSON.parse(localStorage.getItem('servicios'));
						if($scope.servicios)
						swal({
							title: "Error",
							text: "No disponible por problemas de conexión a internet",
							type: "error",
							//html: true,
							showCancelButton: false,
							confirmButtonColor: "#24499C",
							confirmButtonText: 'Reintentar',
							closeOnConfirm: false,
							closeOnCancel: false 
						})
						.then( 
							function() {
								swal({
									title: "Su Turno Móvil",
									text: "Un momento, estamos cargando informacion de las empresas disponibles",
									type: "warning",
									showConfirmButton: false,
									//html: true
								});
								$scope.bloqueaAlerta = false;
								$('body').removeClass('ui-loading');
								$scope.serviciosRefresh();
								
								setTimeout(function() {
									mRefresh.resolve();
								}, 2000);
							}
						);
					}
			    }
			);

			return true;
		}
	}
	$scope.serviciosRefresh();

	

    $scope.servicioClick = function(empresa, servicio, logo, empresa_nombre) {

    	console.log('servicioClick');
    	console.log('empresa_id: ', empresa);

    	$scope.glob_empresa_id = empresa;

    	$scope.atencionCancelada = false;
    	$scope.mapaCargado = false;
    	$scope.servicios_empresa = [];
        $scope.servicio_actual = "";

    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
				title: "Error!",
		    	text: "Sin conexión a internet",
		    	type: "error",
		    	//html: true
		    });
            return false;
    	}

    	$('body').addClass('ui-loading');

    	$scope.logo = logo;
    	localStorage.setItem("logo", logo);

    	$scope.servicio_id_actual = servicio;
    	$scope.empresaActual = empresa;

    	var servicio_detalle = [];

    	var serviciosEmpresaCache = localStorage.getItem('sercicios_' + empresa);

    	if(serviciosEmpresaCache != null && serviciosEmpresaCache != "undefined") {
    		$scope.servicios = JSON.parse(localStorage.getItem('sercicios_' + empresa));
    		$scope.requicitos = localStorage.getItem('msjServicio_' + empresa);
    	}

    	$http({
		   method: 'GET',
		   url: $scope.config.apiurl + "/servicios/empresa/" + empresa,
		   cache: false,
		   timeout: 20000
		})
		.then(
    		function(response2) { // success
    			console.log("consulta ws /servicios/empresa/" + empresa);
    			console.log(response2);

	        	// $scope.servicios_empresa = response2.data;
	        	// $scope.requicitos = response2.data[0].msjServicio;
	        	$scope.isOnline = true;

	        	// actaliza cache solo si es distino
	        	if(JSON.stringify(response2.data) != JSON.stringify(serviciosEmpresaCache)) {
	        		localStorage.setItem('servicios_' + empresa, JSON.stringify(response2.data));
		    		localStorage.setItem('msjServicio_' + empresa, response2.data[0].msjServicio);	
	        	}

		    	// si es primera vez y no tiene datos los carga desde el el servicio internet
		    	if(serviciosEmpresaCache == null || serviciosEmpresaCache == "undefined" || serviciosEmpresaCache == "") {
		    		$scope.servicios_empresa = response2.data;
		    		$scope.requicitos = response2.data[0].msjServicio;
		    	}
		    	
		    	console.log("cambio a direcciones");
		    	$('body').removeClass('ui-loading');
		    	
		    	$.mobile.pageContainer.pagecontainer("change", "#detalles", {
			        transition: "slidefade",
			        reverse: false
			    });

			    setTimeout(function() {
					$scope.$apply();
				}, 100);
	    	}, function(error) { //error
				$scope.isOnline = false;
				$('body').removeClass('ui-loading');
				console.log("obtengo error");
				console.log(error);
	    	}
	    );
    };

    $scope.sucursalesClick = function(servicio, empresa, servicio_actual) {

    	console.log('sucursalesClick');
    	console.log('empresa_id: ', $scope.glob_empresa_id);
    	console.log('servicio_id: ', empresa);

    	$scope.glob_servicio_id = empresa;

    	$scope.mapaCargado = false;
    	$scope.atencionCancelada = false;

    	$scope.programa_mapa = "blank.html";

    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
				title: "Error!",
		    	text: "Sin conexión a internet",
		    	type: "error",
		    	//html: true
		    });
            return false;
    	}

    	$('body').addClass('ui-loading');

    	$scope.direcciones = [];
    	$scope.servicio_actual = servicio_actual;
    	$scope.sercicioAct = servicio;
    	$scope.empresa_id_actual = empresa;
    	//Se setean las variables de localstorage
    	localStorage.setItem("servicio_id_actual", servicio);
    	localStorage.setItem("empresa_id_actual", empresa);

    	$scope.dataServicios = [];
    	
    	$http({
		   method: 'GET',
		   url: $scope.config.apiurl + "/servicios/empresa/" + $scope.glob_empresa_id,
		   cache: false,
		   timeout: 30000
		})
		.then(
			function(response2) {
		       	var dataServicios = response2.data;
		       	// $scope.muestraTee = $scope.dataServicios[$scope.empresa_id];

		       	$scope.serviciosEmpresaActualArray = response2.data;

		       	$scope.dataServicios = dataServicios;
		       	$scope.isOnline = true;

		       	var lat = localStorage.getItem('gps_latitud') || 0;
		       	var lon = localStorage.getItem('gps_longitud') || 0;
		       	
		       	console.log("tengo coordenas GPS?");
		       	console.log(lat + " - " + lon);

		       	// if(lat == null || lat == "" || lat == "undefined") {
	       		// 	console.log("no tengo lat y lon");
	       		// 	lat = 0;
	       		// 	lon = 0;
		       	// }

		       	/*
		    	swal({
						title: "DEBUGGER",
				    	text: "[E]: "+ empresa +"[S]: "+servicio,
				    	type: "warning",
				    	//html: true
				    });	
		    	*/

		    	$http({
				   method: 'GET',
				   url: $scope.config.apiurl + "/sucursales/servicio/" + empresa + "/" + servicio + "/" + lat + "/" + lon,
				   cache: false,
				   timeout: 30000
				})
				.then(
					function(response) {
				        $scope.direcciones = response.data;
				        $scope.isOnline = true;

				        $('body').removeClass('ui-loading'); 

						setTimeout(function(){
							$.mobile.pageContainer.pagecontainer("change", "#sucursales", {
						        transition: "slidefade",
						        reverse: false
						    });
							$scope.$apply();
						},1);
				    }, 
				    function(data) {
				    	$('body').removeClass('ui-loading'); 
				    	swal("Alerta!", "Servicio no disponible, por favor reintente", "warning");
					    $scope.isOnline = false;
				    }
				);
		    },
		    function(error) {
		    	console.log(error);
		    	$('body').removeClass('ui-loading'); 
		    	swal("Alerta!", "No disponible, por favor reintente", "warning");
		    	$scope.isOnline = false;
		    }
		);
    };

    $scope.direccionClick = function(key, direccion, credenciales, muestraTee) {

    	console.log('direccionClick');
    	console.log('empresa_id: ', $scope.glob_empresa_id);
    	console.log('servicio_id: ', $scope.glob_servicio_id);
    	console.log('direccion_id: ', key);

    	$scope.glob_direccion_id = key;

    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
				title: "Error!",
		    	text: "Sin conexión a internet",
		    	type: "error",
		    	//html: true
		    });
            return false;
    	}

    	$scope.muestraTee = muestraTee;
    	console.log("muestraTee");
		console.log($scope.muestraTee);
    	
    	localStorage.setItem('muestraTee', muestraTee);

    	$scope.atencionCancelada = false;

    	$('body').addClass('ui-loading');

    	localStorage.setItem("direccion_actual", JSON.stringify(direccion));
    	localStorage.setItem("gps_direccion", JSON.stringify(direccion));

    	$scope.direccionKey = key;
    	$scope.horario = direccion.horario;
    	$scope.direccion_actual = direccion;
    	$scope.sucursal_actual = direccion.id;
    	$scope.mensaje1 = direccion.mensaje1;
    	$scope.mensaje2 = direccion.mensaje2;
    	$scope.galeria = direccion.galeria;
    	$scope.distanciaActual = direccion.distancia;
    	localStorage.setItem('distanciaActual', direccion.distancia);

    	if(credenciales) {
    		$scope.credenciales = credenciales;
    		localStorage.setItem("credenciales", credenciales);
    	} else {
    		$scope.credenciales = localStorage.getItem("credenciales");
    	} 

    	$scope.formulario = false;
    	$scope.sRut = false;	
    	$scope.sCel = false;
    	$scope.sFijo = false;
    	$scope.sOrd = false;

    	$scope.tRut = "Rut";
    	$scope.tCel = "Celular";
    	$scope.tFij = "Fono Fijo";
    	$scope.tOrden = "N&deg; Orden";

    	switch(credenciales) {
    		case 'RUTCELULAR':
    			$scope.sRut = true;
    			$scope.sCel = true;
    			$scope.sFij = false;
    			$scope.sOrd = false;
    			break;
    		case 'RUT':
    			$scope.sRut = true;
    			$scope.sCel = false;
    			$scope.sFij = false;
    			$scope.sOrd = false;
    			break;
    		case 'CELULAR':
    			$scope.sRut = false;
    			$scope.sCel = true;
    			$scope.sFij = false;
    			$scope.sOrd = false;
    			break;
    		case 'FIJO':
    			$scope.sRut = false;
    			$scope.sCel = false;
    			$scope.sFij = true;
    			$scope.sOrd = false;
    			break;
    		case 'RUTF12':
    			$scope.sRut = true;
    			$scope.sOrd = true;
    			$scope.sCel = false;
    			$scope.sFij = false;
    			$scope.sOrd = false;
    			break;
    		default:
    			$scope.sRut = false;
    			$scope.sCel = false;
    			$scope.sFij = false;
    			$scope.sOrd = false;
    		break;
    	}

    	if(!$scope.tee) {
    		$scope.tee = "N/D";
    		$scope.tee2 = "N/D";
    	}

    	setTimeout(function(){
    		console.log("cargo mapa y calculo la ruta");
    		var timestamp = new Date().getTime();
    		$scope.programa_mapa = "mapa.html?" + timestamp;
			$scope.$apply();
		}, 100);

    	// crea formulario
    	if(credenciales == "NADA") {
	    	
	    	console.log("sin credenciales");
	    	$scope.formulario = false;

	    	$('body').removeClass('ui-loading');

		    setTimeout(function(){
		    	$scope.actualiza();
		    	$.mobile.pageContainer.pagecontainer("change", "#ticket", {
			        transition: "slidefade",
			        reverse: false
			    });
				$scope.$apply();
			},1);

		} else {

			console.log("con credenciales");
			$scope.formulario = true;

			$scope.formu = {};
			$scope.formu.userRut = localStorage.getItem('userRut');
			if(!$scope.formu.userRut || $scope.formu.userRut == "null" || $scope.formu.userRut=="undefined") {
				$scope.formu.userRut = "";
			}
			$scope.formu.numCelular = parseInt(localStorage.getItem('numCelular'));
			$scope.formu.numFijo = parseInt(localStorage.getItem('numFijo'));
			$scope.formu.numOrden = parseInt(localStorage.getItem('numOrden'));

			$('body').removeClass('ui-loading');

		    setTimeout(function(){
		    	$scope.actualiza();
		    	$.mobile.pageContainer.pagecontainer("change", "#ticket", {
			        transition: "slidefade",
			        reverse: false
			    });
				$scope.$apply();
			},1);

			$scope.mapaCargado = true;

			setTimeout(function() {
				// levanta popup con requicitos
				if($scope.requicitos) {
				    swal({
						title: "<div style='margin:0;padding:5px;font-size:16px;'>Mensaje</div>",
				    	text: "<div style='padding:10px;font-size:13px;text-align:left;'>" + $scope.requicitos + "</div>",
				    	//html: true
				    });
				}
			    $("#tabs").tabs( "option", "active", 1 );

			}, 1000);
		}
    };

    $scope.tomaNumero = function() {

    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
                title:"Alerta!",
                text: "Sin conexión a internet",
                type: "warning",
                //html: true
            });
            return false;
    	}

    	// $('body').addClass('ui-loading');

    	//$scope.actualiza();
    	/*
    	swal({
                title: "DEBUGGER",
                text: "[R] "+$scope.formulario+"[B] "+$scope.bloqueaTomaNumero,
                type: "warning",
                //html: true
            });
    	*/
    	if(!$scope.formulario) {

    		// no solicita parametros
    		$scope.bloqueaTomaNumero = false; //Al tener bloqueado el "toma número", nunca hace nada cuando se pide por 2da vez en la misma emp/sucu/serv
    		$scope.tomaNumero2();

    	} else {
	    	if($scope.tee != "-1") {

	    		$scope.bloqueaTomaNumero = false;
	    		$scope.bloqueaCancelar = false;
	    		$scope.bloqueaMeAtendieron = false;
	    		$('body').removeClass('ui-loading');  
	    		
		    	$.mobile.pageContainer.pagecontainer("change", "#pideNumero", {
			        transition: "slidefade",
			        reverse: false
			    });
			    setTimeout(function(){
					$scope.$apply();
				},1);
			} else {
				$('body').removeClass('ui-loading');   
				$scope.$apply(function() {
					swal({
		                title: "Alerta!",
		                text: "No es posible tomar número en este momento",
		                type: "warning",
		                //html: true
		            });
				});
			}
		}
    }


    $scope.popupLogin = function(errorMessage) {

    	var htmlText;
    	if (!errorMessage) {
    		htmlText = 'Email <input type="email" id="email" class="swal2-input">' +
		    'Contraseña <input type="password" id="password" class="swal2-input">';
    	} else {
    		htmlText = '<div style="color:red;">' + errorMessage + ' <a href onclick="angular.element(document.querySelector( \'#pagina_blanca\' )).scope().enviaNuevaPass()">Recuperar Clave</a><br></div>' + 
    		'Email <input type="email" id="email" class="swal2-input">' +
		    'Contraseña <input type="password" id="password" class="swal2-input">';
    	}

    	return swal({
			  title: 'Login requerido',
			  showCancelButton: true,
			  showCloseButton: true,
			  html: htmlText,
		      cancelButtonText: '<span style="font-size:x-small;">Registrarse</span>',
		      confirmButtonText: '<span style="font-size:x-small;">Iniciar Sesión</span>',
		      useRejections: true,
		      reverseButtons: true,
			  preConfirm: function () {
			    return new Promise(function (resolve) {
			      resolve({
			        email: $('#email').val(),
			        password: $('#password').val(),
			        action: "login"
			      })
			    })
			  },
			  onOpen: function () {
			    if ($scope.validateEmail($scope.form.email)) {
			    	$('#email').val($scope.form.email);
			    }
			  }
		})
    	.then( function(result) {
    		return result;
    	},
    	function (result) {
    		if (result != "cancel") {
    			return Promise.reject("cancelado");
    		} else {
    			return {email: $('#email').val(), password: $('#password').val(), action: "register"};
    		}
    	})
    }


    $scope.validateLogin = function(errorMessage){
    	var logeado = localStorage.getItem("logeado");
	    if (logeado == "true") {
	    	return Promise.resolve(true);
	    } else {
	    	return $scope.popupLogin(errorMessage)
			.then(function (result) {

				var email = result.email;
				var password = result.password;
				var action = result.action;

				if (!$scope.validateEmail(email)) {
					return {data: {error:1, msg: "email inválido"}}
				}

				$scope.form.email = email;

				if (!email||!password) {
					return {data: {error:1, msg: "usuario y password son obligatorios"}};
				}

				window.plugins.OneSignal.getIds(function(ids) {
	        		localStorage.setItem("push_id", ids.userId);
	        		console.log(ids.userId);
		        });

		        var pushid = localStorage.getItem("push_id");

	        	var formData = {
					email: email,
					password: password,
					push_id: pushid
				};


				// si fue presionado iniciar sesion o registrar en el modal
				var url;
				if (action === "register") {
					url = $scope.config.apiurl + '/signup';
				} else {
					url = $scope.config.apiurl + '/login';
				}

	        	return $http({
			    	beforeSend: function() { $.mobile.showPageLoadingMsg(); }, //Show spinner
		            complete: function() { $.mobile.hidePageLoadingMsg() }, //Hide spinner
				    method: 'POST',
				    url: url,
				    data: formData,
				    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				    transformRequest: function(obj) {
				      var str = [];
				      for(var p in obj)
				      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				      return str.join("&");
				    },
				    timeout: 30000
				})
	    	})
			.then( function (response) {
				if (response.data.error == 0) {
					$scope.logeado = "true";
					localStorage.setItem("logeado", "true");
					localStorage.setItem("email", $scope.form.email);
					return true;
				} else {
					return $scope.validateLogin(response.data.msg);
				}
			})
		}
    }

    $scope.tomaNumero2 = function() {

    	$('body').removeClass('ui-loading');
    	
    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
                title: "Alerta!",
                text: "Sin conexión a internet",
                type: "warning",
                //html: true
            });
            return false;
    	}
    	
    	var valido = true;
    	var pin = "";

    	// valida rut
    	if($scope.sRut === true) {
	    	if($scope.formu.userRut.length <= 7) {
	    		msgError("El rut ingresado es incorrecto");
	    		valido = false;
	    		$scope.bloqueaTomaNumero = false;
		        return false;
	    	}
	    	if(!$scope.formu.userRut) {
	    		msgError("El rut ingresado es incorrecto");
		        valido = false;
		        $scope.bloqueaTomaNumero = false;
		        return false;
	    	}
	    	if($scope.formu.userRut) {
	    		var rutValido = validaRut($scope.formu.userRut);
	    		if(!rutValido) {
				    msgError("El rut ingresado es incorrecto");
				    valido = false;
				    $scope.bloqueaTomaNumero = false;
		            return false;
	    		}
	    	}
	    	pin = $scope.formu.userRut;
	    }

    	if($scope.sCel === true) {
    		var textPin = $scope.formu.numCelular; 
    		valido = true;
    		var resp = validaCELULAR(textPin);
    		if(resp != "") {
    			valido = false;
    			$scope.bloqueaTomaNumero = false;
    			msgError(resp);
    			return false;
    		}
    		if($scope.sRut === true) {
    			pin = pin + "," + $scope.formu.numCelular;
    		} else {
    			pin = $scope.formu.numCelular;
    		}
	    }

	    if($scope.sFij === true) {
    		valido = true;
    		var resp = validaFIJO($scope.formu.numFijo);
    		if(resp != "") {
    			valido = false;
    			$scope.bloqueaTomaNumero = false;
    			msgError(resp);
    			return false;
    		}
    		pin = $scope.formu.numFijo;
	    }

	    if($scope.sOrd === true) {
	    	valido = true;
    		var rut = $scope.formu.userRut; 
    		var orden = $scope.formu.numOrden; 
    		var resp = validaRUTF12(rut, orden);
    		if(resp != "") {
    			valido = false;
    			$scope.bloqueaTomaNumero = false;
    			msgError(resp);
    			return false;
    		}
    		pin = rut + orden;
	    }

	    if(valido && $scope.bloqueaTomaNumero == false) {

	    	$scope.bloqueaTomaNumero = true;

		    localStorage.setItem('userRut', $scope.formu.userRut);
		    localStorage.setItem('numCelular', $scope.formu.numCelular);
		    localStorage.setItem('numFijo', $scope.formu.numFijo);
		    localStorage.setItem('numOrden', $scope.formu.numOrden);

		    var plataforma = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? 2 : (navigator.userAgent.match(/Android/i)) == "Android" ? 1 : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? 3 : "null";

		    if($scope.teeMin != "undefined") {
		    	var tee_val = $scope.teeMin;
		    } else {
		    	var tee_val = 0;
		    }


		    return $scope.validateLogin(false)
			.then( function(exito) {
				if (!exito) {
					return swal("Se produjo un error al hacer login")
					.then( function () {
						return Promise.reject("error en login");
					})
				} else {
				    /*Se llama directo al local storage para obtener la empresa y el servicio*/
			    	var formData = {
			    		email: localStorage.getItem("email"),
			    		push_id: localStorage.getItem("push_id"),
				    	empresa_id: localStorage.getItem('empresa_id_actual'),//$scope.glob_empresa_id,
						sucursal_id: $scope.sucursal_actual,
						servicio_id: localStorage.getItem('servicio_id_actual'),//$scope.glob_servicio_id,
						tee: tee_val,
						pin: pin,
						plataforma: plataforma
				    };
			    	return $http({
					    method: 'POST',
					    url: $scope.config.apiurl + "/ticket/",
					    data: formData,
					    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					    transformRequest: function(obj) {
					      var str = [];
					      for(var p in obj)
					      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					      return str.join("&");
					    },
					    timeout: 10000
					})
				}
			})
			.then(
				function(resp) {
					$("#bot_volver").hide();

					var r = resp.data;
					
					if(r.codigoError === 0) {

						if(r.numero > 0) {

							$scope.letra = r.letra_servicio;
							$scope.numero_actual = r.numero;
							localStorage.setItem('letra', r.letra_servicio);
							localStorage.setItem('numero_actual', r.numero);

							localStorage.setItem("ticket_actual", r.letra_servicio + r.numero);

							$scope.atencion_terminada = false;

							$scope.nNumeroActual = r.numero;
							localStorage.setItem("nNumeroActual", r.numero);

							$scope.miNumero = r.letra_servicio + r.numero;

							$scope.showTicket = true;
							$scope.showPideTicket = false;
							$scope.showBotVolver = false;
							$scope.bloqueaTomaNumero = true;

							setUltimaAtencion();

							// console.log("cargo mapa y calculo la ruta");
					    	var timestamp = new Date().getTime();
					    	$scope.programa_mapa = "blank.html?" + timestamp;
					    	$scope.programa_mapa_ticket = "mapa.html?" + timestamp;

							// $('#ticket').page();
							$("#t1").removeClass("ui-btn-active");
							$("#t2").removeClass("ui-btn-active");
							$("#t3").removeClass("ui-btn-active");
							$("#t4").removeClass("ui-btn-active");
							$("#t2").addClass("ui-btn-active");
							$('#ticket').page();
						    $("#tabs").tabs( "option", "active", 1 );

							$.mobile.pageContainer.pagecontainer("change", "#ticket", {
						        transition: "slidefade",
						        reverse: true
						    });
							swal.close();
							setTimeout(function(){
								$scope.$apply();
							},1);
							$('body').removeClass('ui-loading');

							// reloj de 8 hrs para cerrar la atencion si el usuario no hace nada
							console.log("reloj iniciado para cierre por timeout");
							clearTimeout(reloj);
							
							var horaIni = new Date();
							localStorage.setItem('relojInicio', horaIni);

							$scope.actualiza();
						}
					}

					if(r.codigoError === 1) {
						
						$scope.atencion_terminada = true;
						$scope.bloqueaTomaNumero = false;
						
						if(r.mensajeError != "" && r.mensajeError != "undefined" && r.mensajeError != null) {
				            swal({
				    			title: "Alerta!",
				    			text: r.mensajeError,
				    			type: "warning",
				    			//html: true
				    		});
				        }

						$scope.showTicket = false;
						$scope.showPideTicket = true;
						$scope.showBotVolver = true;
						$("#bot_volver").show();
						localStorage.removeItem("ticket_actual");
						$scope.tee = "N/D";
						$scope.tee2 = "N/D";
						// swal.close();
						$('body').removeClass('ui-loading');
						$scope.actualiza();
					}

				}, function(error) {

					console.log("error capture general");
					console.log(JSON.stringify(error))

					$scope.atencion_terminada = true;
					$scope.bloqueaTomaNumero = false;

					$scope.showTicket = false;
					$scope.showPideTicket = true;
					$scope.showBotVolver = true;
					$("#bot_volver").show();
					localStorage.removeItem("ticket_actual");
					$scope.tee = "N/D";
					$scope.tee2 = "N/D";
					$('body').removeClass('ui-loading');
					$scope.actualiza();
					if (error != "cancelado") {
						swal({	
			    			title: "Alerta!", 
			    			text: "Servicio no disponible, por favor reintente",
			    			type: "warning",
			    			//html: true
			    		});
					}
				}
			);  // $http.post
		} // if valido
    }

    $scope.popUpCancelar = function() {
    	
    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
				title: "Error!",
		    	text: "Sin conexión a internet",
		    	type: "error",
		    	//html: true
		    });
            return false;
    	} else {

	    	$scope.bloqueaAlerta = true;
	    	swal({
				title: "Alerta",
				text: "¿Está seguro que desea cancelar su turno?",
				type: "warning",
				//html: true,
				showCancelButton: true,
				confirmButtonColor: "#24499C",
				confirmButtonText: 'Si',
				cancelButtonText: 'Volver'
			}).then( function() {
				swal({
					title: 'Un momento!',
					text: 'Espere mientras realizamos la cancelación',
					type: 'warning',
					//html: true,
					showConfirmButton: false,
					showCancelButton: false,
					timer: 3000
				});
	  			$scope.bloqueaAlerta = false;
	  			$scope.showUltimaAtencion = true;
	  			$scope.cancelaAtencion();
	  		}, function() {
				$scope.bloqueaAlerta = false;
				swal.close();
			});
		}

    }

    $scope.popUpMeAtendieron = function() {

    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
				title: "Error!",
		    	text: "Sin conexión a internet",
		    	type: "error",
		    	//html: true
		    });
            return false;
    	} else {

	    	$scope.bloqueaAlerta = true;
	    	$scope.bloqueaTomaNumero = true;
    		$scope.bloqueaCancelar = true;

			return swal({
				title: "Alerta",
				text: "¿Confirma que ya fue atendido?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#24499C",
				confirmButtonText: 'Si',
				cancelButtonText: 'Volver'
			})
			.then( 
				function() {
					$scope.meAtendieron();
					$scope.bloqueaAlerta = false;
					$scope.showUltimaAtencion = true;	
					swal.close();
				},
				function() {
					$scope.bloqueaAlerta = false;
					swal.close();
				}
			);
		}
    }

    $scope.meAtendieron = function() {

    	$scope.bloqueaTomaNumero = true;
    	$scope.bloqueaCancelar = true;

    	localStorage.setItem('miNumero', $scope.miNumero);
    	localStorage.setItem('logo', $scope.logo);
    	
		$scope.miNumero = "---";
		localStorage.removeItem("ticket_actual"); 
		localStorage.removeItem('servicio_id_actual');
		localStorage.removeItem('empresa_id_actual');
		$scope.empresa_id_actual = "";
		$scope.servicio_id_actual = "";
		localStorage.removeItem('horario');
		localStorage.removeItem('direccion_actual');
		localStorage.removeItem('credenciales');
		localStorage.removeItem('logo');
		localStorage.removeItem('nNumeroActual');
		$scope.showBotVolver = true;
		$scope.showTicket = false;
		$scope.showPideTicket = true;
		$scope.atencion_terminada = true;

		$scope.cron = 0;

		// getUltimaAtencion();

		$scope.showUltimaAtencion = true;
		
		$.mobile.pageContainer.pagecontainer("change", "#servicios", {
	        transition: "slidefade",
	        reverse: true
	    });

	    // clearTimeout(reloj);

		setTimeout(function(){
			$scope.$apply();
		},1);
    };

    $scope.cancelaAtencion = function() {

    	$scope.atencionCancelada = true;
    	$scope.bloqueaTomaNumero = true;
    	$scope.bloqueaCancelar = true;

    	var pushid = localStorage.getItem("push_id");

    	var empresa_id = localStorage.getItem('empresa_id_actual');
        var servicio_id = localStorage.getItem('servicio_id_actual');
    	
    	var formData = {
	    	empresa_id: empresa_id,
			sucursal_id: $scope.sucursal_actual,
			servicio_id: servicio_id,
			numero: $scope.numero_actual,
			letra: $scope.letraServicioActual,
			pushid: pushid
	    };

	    if(angular.element('#conectado').val() != "desconectado") {
		    $http({
			    method: 'POST',
			    url: $scope.config.apiurl + "/anular",
			    data: formData,
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			    transformRequest: function(obj) {
			      var str = [];
			      for(var p in obj)
			      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			      return str.join("&");
			    },
			    timeout: 8000
			}).then(function(resp) { // success
				var r = resp.data;
			});
		}

		// cancela haga o no haga la cancelacion en el rest
		setUltimaAtencion();
		$scope.miNumero = "---";
		localStorage.removeItem("ticket_actual"); 
		localStorage.removeItem('servicio_id_actual');
		localStorage.removeItem('empresa_id_actual');
		localStorage.removeItem('horario');
		localStorage.removeItem('direccion_actual');
		localStorage.removeItem('credenciales');
		localStorage.removeItem('logo');
		localStorage.removeItem('nNumeroActual');
		$scope.empresa_id_actual = "";
		$scope.servicio_id_actual = "";

		$scope.showTicket = false;
		$scope.showPideTicket = true;
		$scope.atencion_terminada = true;
		// getUltimaAtencion();

		$scope.showBotVolver = true;
		$scope.bloqueaTomaNumero = false;
		$scope.bloqueaCancelar = false;
		$scope.atencionCancelada = false;

		$scope.showUltimaAtencion = true;

		$scope.glob_empresa_id = null;
		$scope.glob_servicio_id = null;

		setTimeout(function() {
	    	$.mobile.pageContainer.pagecontainer("change", "#servicios", {
		        transition: "slidefade",
		        reverse: true
		    });
		    // swal.close();
			$scope.$apply();
		}, 2000);
    }

    /* Login */
    $scope.submitLogin = function() {

    	var valido = true;
    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
				title: "Error!",
		    	text: "Sin conexión a internet",
		    	type: "error",
		    	//html: true
		    });
            valido = false;
            $('body').removeClass('ui-loading'); 
            return false;
    	}

    	if(!$scope.form.email || !$scope.form.password) {
    		swal({
		      title: 'Su Turno Móvil',
		      text: 'Usuario y clave son obligatorios',
		      type: 'error',
		      //html: true
		    });
		    valido = false;
		    $('body').removeClass('ui-loading'); 
		    return false;
    	}

    	if(!$scope.validateEmail($scope.form.email)) {
			swal({
		      title: 'Su Turno Móvil',
		      text: 'Formato email incorrecto',
		      type: 'error',
		      //html: true
		    });
		    valido = false;
		    $('body').removeClass('ui-loading'); 
		    return false;
    	}

    	$('body').addClass('ui-loading');
    	$scope.logeado = "false";

        if(valido) {

        	window.plugins.OneSignal.getIds(function(ids) {
        		localStorage.setItem("push_id", ids.userId);
        		console.log(ids.userId);
	        });

	        var pushid = localStorage.getItem("push_id");

        	var formData = {
				email: $scope.form.email,
				password: $scope.form.password,
				push_id: pushid
			};

        	$http({
		    	beforeSend: function() { $.mobile.showPageLoadingMsg(); }, //Show spinner
	            complete: function() { $.mobile.hidePageLoadingMsg() }, //Hide spinner
			    method: 'POST',
			    url: $scope.config.apiurl + '/login',
			    data: formData,
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			    transformRequest: function(obj) {
			      var str = [];
			      for(var p in obj)
			      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			      return str.join("&");
			    },
			    timeout: 30000
			})
			.then(
				function(resp) { // success
					console.log("encontre usuario?");
					if (resp.data.error == 0) {

						// carga o actualiza servicios
						$scope.serviciosRefresh();

						$scope.logeado = "true";
						localStorage.setItem("logeado", "true");
						localStorage.setItem("email", $scope.form.email);
						setTimeout(function() {
					    	$scope.$apply(function() {
					    		$scope.logeado = localStorage.getItem("logeado");
					    	});
						}, 500);
						$('body').removeClass('ui-loading'); 

						$.mobile.pageContainer.pagecontainer("change", "#servicios", {
						    transition: "slidefade",
						    reverse: false
						});	
						
						setTimeout(function(){
							$scope.$apply();
						},1);
					} else {
						localStorage.setItem("logeado", "false");
						$scope.logeado = "false";
						$('body').removeClass('ui-loading'); 
						if(resp.data.msg != null && resp.data.msg != "" && resp.data.msg != "undefined") {
							return swal({
								title: "Su Turno Móvil",
								text: resp.data.msg,
								type: "warning",
								showCancelButton: true,
								confirmButtonColor: "#24499C",
								confirmButtonText: "Recuperar Clave"
							}).then( function() {
								if(angular.element('#conectado').val() == "desconectado") {
						    		swal({
										title: "Error!",
								    	text: "Sin conexión a internet",
								    	type: "error",
								    });
						    	} else {
						    		$scope.enviaNuevaPass();
						    	}
							});
						}
					}
				},
				function(error) {
					console.log(error);
					$('body').removeClass('ui-loading'); 
					localStorage.setItem("logeado", "false");
					$scope.logeado = "false";
					swal("Error!", "No se encuentra disponible, por favor reintente", "error");
				}
			);
		}
    };

    $scope.enviaNuevaPass = function() {
    	
    	var formData = {
    		email: $scope.form.email
	    };

    	// rest para obtener nueva pass
	    $http({
		    method: 'POST',
		    url: $scope.config.apiurl + "/restorepass/",
		    data: formData,
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		    transformRequest: function(obj) {
		      var str = [];
		      for(var p in obj)
		      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		      return str.join("&");
		    }
		}).then(
			function(resp) {
				if (swal.isVisible()) {
					swal.getCloseButton().click();
				}
				if(resp.data.error == 1) {
					swal({
						title: 'Su Turno Móvil',
						text: 'El correo electronico no está registrado',
						type: 'success',
						//html: true
				    });
				} else {
					swal({
						title: 'Su Turno Móvil',
						text: 'Hemos enviado un email con su nueva clave',
						type: 'success',
						//html: true
				    });	
				}
			},
			function(error) {
				console.log(error);
			}
		);
    }


    $scope.submitRegistro = function() {

    	if(angular.element('#conectado').val() == "desconectado") {
    		swal({
				title: "Error!",
		    	text: "Sin conexión a internet",
		    	type: "error",
		    	//html: true
		    });
		    $('body').removeClass('ui-loading'); 
            return false;
    	}

    	if(!$scope.form.email || !$scope.form.password) {
    		swal({
		      title: 'Su Turno Móvil',
		      text: 'Usuario y clave son obligatorios',
		      type: 'error',
		      //html: true
		    });
		    valido = false;
		    $('body').removeClass('ui-loading'); 
		    return false;
    	}

    	if(!$scope.validateEmail($scope.form.email)) {
			swal({
		      title: 'Su Turno Móvil',
		      text: 'Formato email incorrecto',
		      type: 'error',
		      //html: true
		    });
		    valido = false;
		    $('body').removeClass('ui-loading'); 
		    return false;
    	}

    	$('body').addClass('ui-loading');
    	$scope.logeado = "false";

		window.plugins.OneSignal.getIds(function(ids) {
    		localStorage.setItem("push_id", ids.userId);
        });

		var pushid = localStorage.getItem("push_id");

		var formData = JSON.stringify({
			email: $scope.form.email,
			password: $scope.form.password,
			push_id: pushid
		});

		// registra al usuario para la app
		$.ajax({
			type: 'POST',
			url: $scope.config.apiurl + '/signup',
			data: formData,
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			timeout: 5000,
			success: function(data) {
				console.log(data);
				if (data.error == 0) {
					$scope.logeado = true;
					localStorage.setItem("logeado", "true");
					localStorage.setItem("email", $scope.form.email);
					swal({
						title: "Mensaje", 
						text: "Registrado con éxito!",
						type: "success",
						//html: true
					});
					$.mobile.pageContainer.pagecontainer("change", "#servicios", {
				        transition: "slidefade",
				        reverse: false
				    });
				    setTimeout(function(){
						$scope.$apply();
					},1);
				} else {
					localStorage.setItem("logeado", "false");

					swal("Alerta!", data.msg, "error");
				}
			},
			error: function(data) {
				swal("Alerta!", "No se encuentra disponible, por favor reintente", "error");
			}
		});

		setTimeout(function() {
		    $('body').removeClass('ui-loading');   //remove class
		}, 1000);
    }

    $scope.submitTerminos = function() {
    	
    }

    function setUltimaAtencion() {
		$scope.ultimaAtencion = {};
		$scope.ultimaAtencion.miNumero = $scope.miNumero;
		$scope.ultimaAtencion.date = new Date();
		$scope.ultimaAtencion.servicio_actual = $scope.servicio_actual;
		$scope.ultimaAtencion.servicio_id_actual = $scope.glob_servicio_id;
		$scope.ultimaAtencion.empresa_id_actual = $scope.glob_empresa_id;
		$scope.ultimaAtencion.nNumeroActual = localStorage.getItem('nNumeroActual');
		$scope.ultimaAtencion.letraServicioActual = $scope.letraServicioActual;
		var direccion = JSON.parse(localStorage.getItem('direccion_actual'));
		if(direccion) {
			$scope.ultimaAtencion.direccion_actual = direccion;
			$scope.ultimaAtencion.horario = direccion.horario;
			$scope.ultimaAtencion.sucursal_actual = direccion.id;
			$scope.ultimaAtencion.mensaje1 = direccion.mensaje1;
			$scope.ultimaAtencion.mensaje2 = direccion.mensaje2;
			$scope.ultimaAtencion.galeria = direccion.galeria;
			$scope.ultimaAtencion.servicio = $scope.servicio_actual;
			$scope.ultimaAtencion.logo = localStorage.getItem("logo");
		}
		$scope.miNumero_ultima = localStorage.getItem('nNumeroActual');
		$scope.showBotVolver = true;
		localStorage.removeItem('ultimaAtencion');
		localStorage.setItem('ultimaAtencion', JSON.stringify($scope.ultimaAtencion));
	}

	function getUltimaAtencion() {
		var dat = localStorage.getItem('ultimaAtencion');
		if(dat !== null) {
			$scope.showUltimaAtencion = true;
			$scope.ultimaAtencion = JSON.parse(dat);
		}
	}

	$scope.showUltimaAtencionBoton = function() {
		$.mobile.pageContainer.pagecontainer("change", "#miUltimoNumero", {
	        transition: "slidefade",
	        reverse: true
	    });
	}

	$scope.goLogout = function() {
		// cambia a pagina especifica
		
		setTimeout(function() {
		    $scope.$apply(function() {
				$scope.logeado = "false";
				localStorage.removeItem("logeado");
			});
		}, 100);
		$.mobile.pageContainer.pagecontainer("change", "#servicios", {
	        transition: "slidefade",
	        reverse: true
	    });
	}

	$scope.goAuth = function() {
		// cambia a pagina especifica
		$.mobile.pageContainer.pagecontainer("change", "#auth", {
	        transition: "slidefade",
	        reverse: true
	    });
	}

	$scope.goInicio = function() {
		// cambia a pagina especifica
		$.mobile.pageContainer.pagecontainer("change", "#servicios", {
	        transition: "slidefade",
	        reverse: true
	    });
	}

	$scope.distancia = function(val) {
		return val;
	}

	$scope.condiciones = function() {
		$.mobile.pageContainer.pagecontainer("change", "#condiciones", {
	        transition: "slidefade",
	        reverse: true
	    });

	}

	/**
	* Navegacion 
	**/

	$scope.goEmpresas = function() {

		$('body').addClass('ui-loading');

		$scope.servicios_empresa = [];

		$.mobile.pageContainer.pagecontainer("change", "#servicios", {
	        transition: "slidefade",
	        reverse: true
	    });

	    setTimeout(function() {
		    $('body').removeClass('ui-loading');   //remove class
		}, 1000);
	}

	$scope.goDetalles = function() {
		$.mobile.pageContainer.pagecontainer("change", "#detalles", {
	        transition: "slidefade",
	        reverse: true
	    });
	};

	$scope.pestanaActual = function(pestana) {
		$scope.pestana = pestana;

		if(pestana == "t6") {
			// document.getElementById('mapa2_cont').contentWindow.actualizaUbicacion();
			$scope.actualizaMapa();
		}
		if(pestana == "t3") {
			// document.getElementById('mapa_cont').contentWindow.actualizaUbicacion();
			$scope.actualizaMapa();
		}
	}

	$scope.refreshPestanaActual = function() {
		// setTimeout(function(){
			// $("#" + $scope.pestana).trigger('click');
			// $("#navbar").navbar();
			// $("#navbar2").navbar();
			$scope.$apply();
		// },1);
	}

	/** google login **/

	// $scope.onSignIn = function (googleUser) {
	// 	var profile = googleUser.getBasicProfile();
	// 	localStorage.setItem('google', profile);
 //    }

 	$scope.actualizaMapa = function() {
 		console.log("actualizo mapa");
 		// console.log("cargo mapa y calculo la ruta");
    	var timestamp = new Date().getTime();
    	$scope.programa_mapa = "mapa.html?" + timestamp;
    	$scope.programa_mapa_ticket = "mapa.html?" + timestamp;
 	};

    $scope.imgClick = function() {
 		return false;
 	};

 	$scope.limpiaMapa = function() {
    	console.log("limpio mapa");
    	$scope.programa_mapa = "mapa.html";
    	var empresa_id = localStorage.getItem('empresa_id_actual');
        var servicio_id = localStorage.getItem('servicio_id_actual');
    	$scope.sucursalesClick(servicio_id, empresa_id, $scope.servicio_actual);
    }

    $scope.actualizaDirecciones = function() {
    	console.log("actualiza direcciones (2)");
    	var empresa_id = localStorage.getItem('empresa_id_actual');
        var servicio_id = localStorage.getItem('servicio_id_actual');
    	$scope.sucursalesClick(servicio_id, empresa_id, $scope.servicio_actual);
    }

	$scope.validateEmail = function(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

	setTimeout(function() {
	    if($scope.miNumero != "---") {
			$scope.showBotVolver = false;
		}
	}, 1000);

	// levanta instancia de actualizaciones cada 30's
    setInterval(function() {
    	$scope.actualiza();

    	if($scope.miNumero != "---") {
	    	var horaInicio = Date.parse(localStorage.getItem('relojInicio'));
			var now = new Date();
			var diffMs = (now - horaInicio);
			var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); 
			console.log("dif: " + diffMins);
			if(diffMins >= $scope.config.cierra_atencion_hrs * 60) {
				swal.close();
				$scope.cancelaAtencion();
			}
		}

    }, $scope.config.tiempo_actualizacion); 
    
    // al iniciar app
    setTimeout(function() {
    	$scope.actualiza();
    }, 2000);

    setTimeout(function() {
    	$scope.$apply(function() {
    		$scope.logeado = localStorage.getItem("logeado");
    	});
	}, 1);

}]);